<?php

/*
|	   __  ___   ______  _______ 
|	 / / / | | / / __ \/  _/ _ \
|	/ /_/ /| |/ / /_/ _/ // // /
|	\____/ |___/\____/___/____/ 	
| 	
|
|	This is the uvoid api. It acts as the point
|	of access for all current applications and sites
|	in the uvoid network.
|
|	Created by
|
|	Elmario Husha
|
|		and
|
|	Dan 	Nash
|	
*/

ww_bootstrap();

$router->get('/', function () use ($router) {
    return ['gang' => 'gang'];
});

// Auth stuff

$router->group([
		'prefix' => 'auth',
		'namespace' => 'Auth'
	], function() use ($router) {
		$router->post('/token', 'TokenController@generate');
	}
);

// Create and login to user accounts

$router->group([
		'prefix' => 'users',
		'namespace' => 'Users',
		'middleware' => 'token'
	], function() use ($router) {

		$router->post('/create', 'UserController@create');
		$router->post('/login', 'UserController@login');
	}
);

if (APP_DEBUG) {

	// For debugging environments, we map all the routes to GET
	// as well as post / others for ease or testing

	$routes = $router->getRoutes();

	foreach ($routes as $route) {
		if ($route['method'] != 'GET')  {
			$action = $route['action'];

			$action['uses'] = str_replace('App\\Http\\Controllers\\', '', $action['uses']);

			$router->get($route['uri'], $action);
		}
	}
}
