<?php

namespace App\Http\Middleware;

use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

use Closure;

use App\Components\Auth\WebTokenHelper;

use App\Components\Responses\Response;
use App\Components\Responses\ErrorResponse;
use App\Components\Responses\UnauthorizedResponse;

class TokenAuthMiddleware
{
	private const IP_WHITELIST = [
		'86.13.18.112'
	];

    public function handle($request, Closure $next)
    {
        if (!( $token = $request->input('token') )) {
            // Didn't find a token

            return (new UnauthorizedResponse('No token found'))->raw();
        }

        $tokenResult = WebTokenHelper::checkToken($token);

        if (isset($tokenResult->original) && $tokenResult->original instanceof ErrorResponse) {
            return $tokenResult->original->raw();
        }

        $request->merge(['_auth' => $tokenResult]);

        return $next($request);
    }
}
