<?php

namespace App\Http\Middleware;

use Closure;

use App\Components\Responses\Response;
use App\Components\Responses\ErrorResponse;

class ResponseParseMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (($ores = $response->original) instanceof Response) {
            
            $response->setStatusCode($ores->getCode());

        } else if ( is_string( ($content = $response->content()) ) ) {
        	// We want to force JSON response

        	if ($response->exception) {
                if (is_production()) {
                    // Let's not make a fool of ourselves in public

                    // Throw a generic error

                    $response->setContent(new ErrorResponse);
                } else {
                    return $response;
                }

        	}

        }

        $response->header('Content-Type', 'application/json');
        return $response;
    }
}
