<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Components\Users\UserManager;

use CreatedResponse;
use DataResponse;
use ErrorResponse;

class UserController extends Controller
{
	public function create(Request $r)
	{
		if (!UserManager::userRequestValid()) {
			return $this->invalidRequest();
		}

		$userId = UserManager::makeUser($r->input('username'), $r->input('password'));

		if (null === $userId) {
			// We caught an error response

			return new ErrorResponse('This username already exists!');
		}

		return new CreatedResponse($userId);
	}

	public function login(Request $r)
	{
		if (!UserManager::userRequestValid()) {
			return $this->invalidRequest();
		}

		$user = UserManager::getUserLogin($r->input('username'), $r->input('password'));

		if (!$user) {
			return new ErrorResponse('Login failed');
		}

		$response = new DataResponse([
			'user' => $user
		]);

		return $response;
	}

	private function invalidRequest()
	{
		return new ErrorResponse('Request requires both username (alphanumeric, 8-16 chars) and password (8-32 chars)');
	}
}