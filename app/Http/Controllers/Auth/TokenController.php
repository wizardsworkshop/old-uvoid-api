<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Components\Auth\WebTokenHelper;

use DataResponse;
use ErrorResponse;
use UnauthorizedResponse;

class TokenController extends Controller
{
    public function generate(Request $r)
    {
        // Generate new token for request auth

        if (!request_ip_valid()) {
            return new UnauthorizedResponse('Invalid Origin');
        }

        $routes = $r->input('routes');

        if (!is_array($routes)) {
            return new ErrorResponse('No routes parameter specified');
        }

        $subject = $r->input('subject', 'uvoid-website');
        $expires = $r->input('expires', 900);

        $token = WebTokenHelper::makeToken($routes, $subject, $expires);

        return new DataResponse([
            'token' => $token
        ]);
    }
}