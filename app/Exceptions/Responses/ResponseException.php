<?php

namespace App\Exceptions\Responses;

use Response;

class ResponseException extends \RuntimeException
{
	protected $response;

	public function __construct(Response &$res, string $message = '', int $code = 0, \Throwable $previous = null)
	{
		parent::__construct($message, $code, $previous);

		$this->response = $res;
	}

	public function getThrownResponseInstance(): Response
	{
		return $this->response;
	}
}