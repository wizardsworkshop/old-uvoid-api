<?php

namespace App\Exceptions\Responses;

use DataResponse;

// Simple exception for when DataResponse data is overwritten (using an existing key to set data)

class ResponseDataOverwrittenException extends ResponseDataException
{
	protected $res;
	protected $dataKey;
	protected $newData;

	public function __construct(DataResponse &$res, string $key, array $newData, string $message = '', int $code = 0, \Throwable $previous = null)
	{
		if ('' === $message) {
			$message = "
				Key '$key' already used in DataResponse instance, 
				please use the force option (with caution) or use 
				this exception's 'forceOverwrite' method.
			";
		}

		$this->res = $res;
		$this->dataKey = $key;
		$this->newData = $newData;

		parent::__construct($res, $message, $code, $previous);
	}

	public function getNewData()
	{
		return $this->newData;
	}

	public function forceOverwrite()
	{
		// If we want we can disregard the exception and use the new data instead

		// Returns the response used

		return $this->res->addData($this->dataKey, $this->newData, true);
	}
}