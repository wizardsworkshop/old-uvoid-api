<?php

namespace App\Exceptions\Responses;

class ResponseDataException extends ResponseException
{
	public function __construct(DataResponse &$res, string $message = '', int $code = 0, \Throwable $previous = null)
	{
		parent::__construct($res, $message, $code, $previous);
	}
}