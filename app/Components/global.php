<?php

/*
|
|  G L O B A L
|
|  These can be used in any script its crazy!
|
*/

use App\Components\DBG;
use App\Components\Responses\Response;

function ww_bootstrap()
{
	// Prevent running more than once

	if (!defined('APP_DEBUG')) {

		define('APP_DEBUG', env('APP_DEBUG'));
		define('REDIS_PREFIX', env('REDIS_PREFIX', 'uvoidapi:'));

		if (APP_DEBUG) {
		
			function dbg($val, $key = null, $kill = false)
			{
				DBG::addMessage($val, $key, $kill);
			}

			function gdbg()
			{
				return DBG::getMessages();
			}

			db()->listen(function($query) {
	    		dbg([
	    			'sql' => $query->sql,
	    			'bind' => $query->bindings
	    		]);
	    	});

		} else {
			// Do nothing when called to avoid errors
			// Also don't have to check env each call
			function dbg($key = null, $val = null, $kill = null) {  }

			function gdbg() { return []; }
		}
	}
}

function db(): Illuminate\Database\DatabaseManager
{
	// I might change this on future to access different connections 
	// so its a nice wrapper to have

	return app('db');
}

function is_production()
{
	return app('env') === 'production';
}

function is_local()
{
	return app('env') === 'local';
}

function request_ip_valid()
{
	$validIps = [
		'86.13.18.*'
	];

	$userIp = app('request')->ip();

	if (is_local() && $userIp == '::1') {
		// Sometimes it returns this testing locally but fuck it

		$userIp = env('LOCAL_SAFE_IP', '86.13.18.112');
	}

	$trimmedUserIp = substr($userIp, 0, strrpos($userIp, '.'));

	foreach ($validIps as $ip) {
		// Check if it's a wildcard IP

		$isWild = (substr($ip, -2) == '.*');

		if (
			($isWild && (substr($ip, 0, -2) == $trimmedUserIp)) ||
			$ip == $userIp
		) {
			return true;
		}
	}

	return false;
}

// Wrapper to validate requests easily

function request_valid(array $validationParams): bool
{
	return !app('validator')->make(
		app('request')->only(array_keys($validationParams)), 
		$validationParams
	)->fails();
}

function redis()
{
	$args = func_get_args();

	$cargs = count($args);

	$redis = app('redis');

	switch ($cargs) {
		case 0:
			return $redis;
			break;
		case 1:
			// Pull the value for a given key
			$val = $redis->get($args[0]);

			if (null !== $val && !is_numeric($val)) {
				$val = unserialize($val);
			}

			return $val;
			break;
		case 2:
			// Set a value in redis with a given key
			$val = $args[1];
			
			if (!is_numeric($val)) {
				$val = serialize($val);
			}

			return $redis->set($args[0], $val);
			break;
		case 3:
			// Set an argument with expiry (second argument, time in seconds)
			$val = $args[2];
			
			if (!is_numeric($val)) {
				$val = serialize($val);
			}

			return $redis->setex($args[0], $args[1], $val);
			break;
		default:
			throw new \InvalidArgumentException('Too many args to global function redis()');
			break;
	}
}