<?php

// D E B U G 
// Container

namespace App\Components;

class DBG
{
	private static $messageBag = [];

	public static function addMessage($val, $key = null, $kill = false)
	{
		if (null === $key) {
			static::$messageBag[] = $val;
		} else {
			static::$messageBag[$key] = $val;			
		}

		if ($kill) {
			dd(static::getMessages());
		}
	}

	public static function getMessages()
	{
		return static::$messageBag;
	}
}