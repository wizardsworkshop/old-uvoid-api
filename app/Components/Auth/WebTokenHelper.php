<?php

namespace App\Components\Auth;

use Firebase\JWT\JWT;

use Exception;

use UnauthorizedResponse;
use ErrorResponse;

class WebTokenHelper
{
	private const ALL_ROUTES_PERMISSIONS = ['*'];

	// Generate a new authentication token

	public static function makeToken(array $validRoutes, string $subject = 'uvoid-website', int $expiresAfter = 900): array
	{
		$now = time();

        $payload = [
            // Issuer of the token
            'iss' => 'uvoid-api',

            // Subject of the token
            'sub' => $subject,
            
            // Time when JWT was issued
            'iat' => $now,
            
            // Expiration time, token lasts for 15 min

            'exp' => ($expires = ($now + $expiresAfter)),

            // Allowed routes

            'routes' => $validRoutes
        ];

        return [
            'value' => JWT::encode($payload, env('JWT_SECRET')),
            'expires' => $expires
        ];
	}

	// Check a token is valid and get ErrorResponse or credentials of token

	public static function checkToken($token, $routePath = null)
	{
		try {

			if (is_array($token)) {
				$token = $token['value'];
			}

            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

            // Check if we have the special 'wildcard' permissions

            if ($credentials->routes === static::ALL_ROUTES_PERMISSIONS) {
            	$credentials->isWildcard = true;
            	return $credentials;
            }

            // Check if the requested path is included in the token's allowed routes

            $path = ($routePath ?: app('request')->path());

            if (!in_array($path, $credentials->routes)) {
                return (new UnauthorizedResponse('Invalid Request (Unauthorized Route)'))->raw();
            }

        } catch(ExpiredException $e) {

            return (new UnauthorizedResponse('Token Has Expired'))->raw();

        } catch(Exception $e) {

            return (new UnauthorizedResponse('Invalid Token'))->raw();

        }

        return $credentials;
	}
}