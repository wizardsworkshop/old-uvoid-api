<?php

namespace App\Components\Responses;

use App\Exceptions\Responses\ResponseDataException;
use App\Exceptions\Responses\ResponseDataOverwrittenException;

// Updated response, contains row count

class DataResponse extends SuccessResponse
{
	private $data = [];

	public function __construct(array $data = [])
	{
		parent::__construct();

		foreach ($data as $key => $vals) {
			if (is_object($vals)) {
				$vals = get_object_vars($vals);
			}

			$this->addData($key, $vals);
		}
	}

	public function addData(string $key, array $vals, bool $force = false)
	{
		if (isset($this->data[$key]) && !$force) {
			throw new ResponseDataOverwrittenException($this, $key, $vals);
		}

		$this->data[$key] = $vals;
		
		return $this;
	}

	public function contents()
	{
		return $this->data;
	}

	public function parse()
	{
		if (empty($this->data)) {
			throw new ResponseDataException($this, 'Response data is empty, please add data before returning or use different response type');
		}

		return [
			'success' => $this->success,
			'data' => $this->data
		];
	}
}