<?php

namespace App\Components\Responses;

use Illuminate\Contracts\Support\Jsonable;

use Illuminate\Http\Response as IlluminateResponse;

// Basic response

class Response implements Jsonable
{
	protected $success = false;

	public const STATUS_CODE = 200;

	public function __construct()
	{
		// Nothing to see here
	}

	// Function to return the response to the user
	// If this is overridden it must return at least
	// the success property

	public function parse()
	{
		return [
			'success' => $this->success
		];
	}

	// Implement the 'Jsonable' interface

	public function toJson($options = 0)
    {
    	$json = $this->parse();

    	$json['meta'] = [];

    	if (app('request')->has('_auth')) {
    		$auth = app('request')->_auth;

			$json['meta']['auth'] = [
				'subject' => $auth->sub,
				'valid_for' => ($auth->exp - time())
			];
    	}

    	if (APP_DEBUG) {
    		$json['meta']['debug'] = gdbg();
    	}

    	return json_encode($json);
    }

    // Convert to an illuminate htto response object

    public function raw(): IlluminateResponse
    {
    	return response($this)->setStatusCode($this->getCode());
    }

    public function getCode(): int
    {
    	return static::STATUS_CODE;
    }
}