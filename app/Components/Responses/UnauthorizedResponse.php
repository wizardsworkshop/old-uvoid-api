<?php

namespace App\Components\Responses;

use Illuminate\Http\Response as IlluminateResponse;

// Unauthorized response

class UnauthorizedResponse extends ErrorResponse
{
	protected $success = null;

	public const STATUS_CODE = 401;
}