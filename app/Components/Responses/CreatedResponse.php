<?php

namespace App\Components\Responses;

// Updated response, contains row count

class CreatedResponse extends SuccessResponse
{
	protected $success = true;

	// Number of rows affected by update
	private $insertId;

	public function __construct(int $insertId = 0)
	{
		parent::__construct();
		$this->insertId = $insertId;
	}

	public function parse()
	{
		return [
			'success' => $this->success,
			'insertId' => $this->insertId
		];
	}
}