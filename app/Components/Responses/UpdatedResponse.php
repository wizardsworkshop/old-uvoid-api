<?php

namespace App\Components\Responses;

// Updated response, contains row count

class UpdatedResponse extends SuccessResponse
{
	protected $success = true;

	// Number of rows affected by update
	private $numRows;

	public function __construct(int $numRows = 0)
	{
		parent::__construct();
		$this->numRows = $numRows;
	}

	public function parse()
	{
		return [
			'success' => $this->success,
			'numRows' => $this->numRows
		];
	}
}