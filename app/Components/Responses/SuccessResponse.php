<?php

namespace App\Components\Responses;

// Successful response

class SuccessResponse extends Response
{
	protected $success = true;

	public function __construct()
	{
		parent::__construct();
	}
}