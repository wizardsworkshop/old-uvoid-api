<?php

namespace App\Components\Responses;

// Error response

class ErrorResponse extends Response
{
	protected $success = false;

	public const STATUS_CODE = 400;

	// Array of errors
	protected $errors;

	public function __construct(...$errors)
	{
		parent::__construct();
		$this->errors = $errors;
	}

	public function parse()
	{
		$errors = count($this->errors) > 0
					? $this->errors
					: ['Unknown error'];

		return [
			'success' => $this->success,
			'errors' => $errors
		];
	}
}