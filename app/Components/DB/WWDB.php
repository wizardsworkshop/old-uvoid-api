<?php

namespace App\Components\DB;

use Illuminate\Database\QueryException;

// Database helper yay

class WWDB
{
	// Get the ID of the inserted value, or return null on error
	public static function insertGetId(string $table, array $values)
	{
		try {
			return db()->table($table)->insertGetId($values);			
		} catch (QueryException $ex) {
			// Log?

			

			return null;
		}
	}

	public static function selectOne(string $table, array $fields = ['*'], array $where = [])
	{
		return db()
			->table($table)
			->select($fields)
			->where($where)
			->first();
	}
}