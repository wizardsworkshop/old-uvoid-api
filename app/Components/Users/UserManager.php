<?php

namespace App\Components\Users;

use Illuminate\Http\Request;

use App\Components\DB\WWDB;

class UserManager
{
	// Returns ID of user, or null if there was an error
	
	public static function makeUser(string $username, string $password)
	{
		return WWDB::insertGetId('users', [
			'username' => $username,
			'password' => static::hash($password)
		]);
	}

	// Return error false if user not found, or user object if found

	public static function getUserLogin(string $username, string $password)
	{
		$user = static::getUserByName($username);

		if (null !== $user && app('hash')->check($password, $user->password)) {
			// User exists and password is valid!

			unset($user->password);

			return $user;
		}

		return false;
	}

	// Return null or user if found with username

	public static function getUserByName(string $username)
	{
		$select = [
			'id', 
			'username', 
			'password', 
			'created_on'
		];

		$where = [
			'username' => $username
		];

		return WWDB::selectOne('users', $select, $where);
	}

	// Validate a user request (must have username and password)

	public static function userRequestValid(): bool
	{
		return request_valid([
			'username' => 'required|between:8,16|alpha_num',
			'password' => 'required|between:8,32'
		]);
	}

	private static function hash(string $str)
	{
		return app('hash')->make($str);
	}
}