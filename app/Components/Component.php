<?php

namespace App\Components;

class Component
{
	// We can debug stuff! I prefer this way of logging
	
	private $debug = APP_DEBUG;

	public function __construct()
	{
		// Nothing!
	}

	protected function setDebug(bool $debug)
	{
		$this->debug = $debug;
	}
}