<?php

use App\Components\Users\UserManager;

// We can override the test username 
// and password with the .env
// depending on the environment

define('TEST_USER_USERNAME', env('TEST_USER_USERNAME', 'Penguin21'));
define('TEST_USER_PASSWORD', env('TEST_USER_PASSWORD', 'ValidPass123'));

use Laravel\Lumen\Testing\DatabaseTransactions;

use App\Components\Auth\WebTokenHelper;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
	use DatabaseTransactions;

	const TEST_USER_USERNAME = TEST_USER_USERNAME;
	const TEST_USER_PASSWORD = TEST_USER_PASSWORD;

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    // Override 'call' to append an auth token to every request

    public function call($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null)
    {
        $tokenParam = [
            'token' => WebTokenHelper::makeToken(['*'])
        ];

        $parameters = array_merge($parameters, $tokenParam);
        return parent::call($method, $uri, $parameters, $cookies, $files, $server, $content);
    }

    // Shortcut for call and parses response to array

    protected function _c(string $method, string $route, array $body = [], $mustBeSuccessful = true)
    {
    	$response = $this->call($method, $route, $body);

    	if ($mustBeSuccessful) {
    		$jsonRes = $this->_parseResponse($response);

    		$this->assertTrue($response->isSuccessful() && $jsonRes['success']);
    		return $jsonRes;
    	}

    	return $this->_parseResponse($response);
    }

    // Shortcut to dataresponse call, should follow correct structue 

    protected function _dc(string $method, string $route, array $body, $mustBeSuccessful = true)
    {
        $resArr = $this->_c($method, $route, $body, $mustBeSuccessful);

        $this->assertArrayHasKeys(['success', 'data'], $resArr);

        $this->assertTrue(
            is_array($resArr['data']) && 
            (count(array_keys($resArr['data'])) > 0)
        );

        return $resArr;
    }

    // Parse JSON string response to array

    protected function _parseResponse(Illuminate\Http\Response $response)
    {
    	return json_decode($response->content(), true);
    }

    protected function makeTestUser()
    {
    	return UserManager::makeUser(static::TEST_USER_USERNAME, static::TEST_USER_PASSWORD);
    }

    // Ensure an array has all of an array of 'needles' (keys)

    protected function assertArrayHasKeys(array $needles, array $haystack)
    {
        $needlecount = count($needles);
        $i = 0;

        $pass = true;
        $message = '';

        if (0 === $needlecount) {
            $pass = false;
            $message = 'Array has no keys, please validate structure or no assertion will be performed';
        }

        while ($pass && $i < $needlecount) {
            $pass = array_key_exists($needles[$i], $haystack);

            if (!$pass) {
                $message = "Failed asserting array key '$needles[$i]' exists in array";
            }

            $i++;
        }

        $this->assertTrue($pass, $message);
    }

    protected function assertObjectHasProps(array $props, $obj)
    {
        $objArr = get_object_vars($obj);
        $this->assertArrayHasKeys($props, $objArr);
    }

    protected function assertIsErrorResponse(array $response)
    {
        $this->assertFalse($response['success']);

        $this->assertNotCount(0, $response['errors']);
    }
}
