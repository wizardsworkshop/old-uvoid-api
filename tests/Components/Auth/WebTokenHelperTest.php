<?php

use App\Components\Auth\WebTokenHelper;

use App\Components\Responses\ErrorResponse;

class WebTokenHelperTest extends TestCase
{
	public function testWebTokenCreate()
	{
		$shouldExpireAt = time() + 1;

		$token = WebTokenHelper::makeToken(['*'], 'test-user', 1);

		$this->assertArrayHasKeys(['value', 'expires'], $token);

		$this->assertEquals($shouldExpireAt, $token['expires']);
	}

	public function testWebTokenCheck()
	{
		$token = WebTokenHelper::makeToken(['*'], 'test-user', 1);

		$credentials = WebTokenHelper::checkToken($token);

		$this->assertObjectHasProps([
			'iss',
			'sub',
			'iat',
			'exp',
			'routes',
			'isWildcard'
		], $credentials);

		$this->assertTrue($credentials->isWildcard);

		sleep(1);

		// Make sure the token expires after one second

		$content = json_decode(WebTokenHelper::checkToken($token)->content());

		$this->assertEquals($content->errors[0], 'Invalid Token');

		// Ensure non-global routing param isn't flagged by mistake

		$token = WebTokenHelper::makeToken(['users/create', 'users/login'], 'test-user', 1);

		$credentials = WebTokenHelper::checkToken($token, 'users/create');

		$this->assertObjectHasProps([
			'iss',
			'sub',
			'iat',
			'exp',
			'routes'
		], $credentials);

		$this->assertFalse(isset($credentials->isWildcard));
	}
}