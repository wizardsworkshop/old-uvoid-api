<?php

use App\Components\Users\UserManager;

class UserManagerTest extends TestCase
{
	public function testUserManagerMakeUser()
	{
		$userId = UserManager::makeUser(
			static::TEST_USER_USERNAME, 
			static::TEST_USER_PASSWORD
		);

		$this->assertTrue(is_integer($userId));

		$user = db()->select('
			SELECT 
				username
			FROM
				users
			WHERE
				id = ?
		', [$userId]);

		$this->assertCount(1, $user);

		$this->assertEquals($user[0]->username, static::TEST_USER_USERNAME);

		// We should make sure null is returned when there's an
		// error with the query (eg. existing username)

		$userId = UserManager::makeUser(
			static::TEST_USER_USERNAME, 
			static::TEST_USER_PASSWORD
		);

		$this->assertNull($userId);
	}

	public function testUserManagerGetUserLogin()
	{
		$userId = $this->makeTestUser();

		$user = UserManager::getUserLogin(
			static::TEST_USER_USERNAME, 
			static::TEST_USER_PASSWORD
		);

		$this->assertObjectHasProps([
			'id', 
			'username', 
			'created_on'
		], $user);

		$this->assertEquals($user->id, $userId);

		$this->assertEquals($user->username, static::TEST_USER_USERNAME);

		// Make sure we can fail!

		$this->assertFalse(UserManager::getUserLogin(
			static::TEST_USER_USERNAME,
			'wrong password!'
		));

		$this->assertFalse(UserManager::getUserLogin(
			'wrong username!',
			static::TEST_USER_PASSWORD
		));
	}
}