<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class CreateUserRouteTest extends TestCase
{
    /**
     * Test users can be created with valid details
     *
     * @dataProvider validUserProvider
     */

    public function testCreateUserRouteSuccess($username, $password)
    {
        $resArr = $this->makeCreatePost($username, $password);

        $this->assertTrue($resArr['success']);

        $this->assertTrue(is_integer($resArr['insertId']));
    }

    /**
     * Test users cannot be created with invalid details (user or pass)
     *
     * @dataProvider invalidUserProvider
     */

    public function testCreateUserRouteInvalidParams($username, $password)
    {
        $resArr = $this->makeCreatePost($username, $password, false);

        $this->assertFalse($resArr['success']);

        $this->assertNotCount(0, $resArr['errors']);

        $this->assertContains('Request requires both username (alphanumeric, 8-16 chars) and password (8-32 chars)', $resArr['errors']);
    }

    /**
     * Test users cannot be created when the username already exists
     */

    public function testCreateUserRouteAlreadyExists()
    {
        $validResponse = $this->makeCreatePost('ThisIsAValidUser', 'ValidPass123');

        // Then we try make the same user again

        $resArr = $this->makeCreatePost('ThisIsAValidUser', 'ValidPass123', false);

        $this->assertIsErrorResponse($resArr);

        $this->assertContains('This username already exists!', $resArr['errors']);
    }

    private function makeCreatePost($username, $password, $mustBeSuccessful = true)
    {
        return $this->_c('POST', '/users/create', [
            'username' => $username,
            'password' => $password
        ], $mustBeSuccessful);
    }

    public function validUserProvider()
    {
        return [
            ['ThisIsAValidUser', 'ValidPass123'],
            ['AntherOne233', 'Pa$$w0rd']
        ];
    }

    public function invalidUserProvider()
    {
        return [
            ['BadUser', 'ValidPass123'],
            ['AntherOne233', 'Pa$$']
        ];
    }
}
