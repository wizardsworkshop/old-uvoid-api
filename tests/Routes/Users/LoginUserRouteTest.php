<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

use App\Components\Users\UserManager;

use App\Components\Responses\DataResponse;

class LoginUserRouteTest extends TestCase
{	
    /**
     * Test users can log in with valid credentials
     *
     * @dataProvider 
     */

    public function testLoginUserRouteSuccess()
    {
    	$this->makeTestUser();

    	$resArr = $this->_dc('POST', '/users/login', [
    		'username' => static::TEST_USER_USERNAME,
    		'password' => static::TEST_USER_PASSWORD
    	]);

    	$data = $resArr['data'];
    	$user = $data['user'];

    	$this->assertTrue(is_integer($user['id']), 'Incorrect user ID format returned, expected int');

    	$this->assertEquals($user['username'], static::TEST_USER_USERNAME, 'Username was incorrect in returned user');

    	$this->assertNotNull($user['created_on'], 'No user timestamp found');
    }

    /**
     * Test error is given for invalid credentials
     *
     * @dataProvider invalidUserProfider
     */

    public function testLoginUserRouteFailure($user, $pass)
    {
    	$errorStr = 'Request requires both username (alphanumeric, 8-16 chars) and password (8-32 chars)';

    	$resArr = $this->_c('POST', '/users/login', [
    		'username' => $user,
    		'password' => $pass
    	], false);

    	$this->assertIsErrorResponse($resArr);

    	$this->assertContains($errorStr, $resArr['errors']);
    }

    public function invalidUserProfider()
    {
    	return [
    		['2short', static::TEST_USER_PASSWORD],
    		[static::TEST_USER_USERNAME, '2short']
    	];
    }
}
